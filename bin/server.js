const app = require("../src/app");
const { BASE_PATH } = require("../src/utils/constants/paths");

const PORT = 3000;

app.listen(PORT, () => {
  console.log(
    `Balder is running on port ${PORT}\nAPI URL: http://localhost:${PORT}${BASE_PATH}`
  );
});
