const mongoose = require("mongoose");

const useDatabase = async (connectionString) => {
  try {
    const connection = await mongoose.connect(connectionString, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    });
    mongoose.Promise = global.Promise;

    console.log("Database connected");

    return connection;
  } catch (err) {
    console.log("Error to connect database");
  }
};

module.exports = useDatabase;
