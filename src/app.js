const dotenv = require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

const express = require("express");
const { CLIENT_ERROR } = require("./utils/constants/status-code");
const {
  BASE_PATH,
  PROFESSIONAL_PATH,
  PROFESSION_PATH,
  EXPERTISE_PATH,
  COUNCIL_PATH,
  LOGIN_PATH,
} = require("./utils/constants/paths");

const useDatabase = require("./database");

const { DB_CONNECTION_STRING } = process.env;
useDatabase(DB_CONNECTION_STRING);

const professional = require("./routers/professional");
const profession = require("./routers/profession");
const expertise = require("./routers/expertise");
const council = require("./routers/council");
const login = require("./routers/login");

const app = express();
const server = express();

app.use(express.json());
app.use(PROFESSION_PATH, profession);
app.use(PROFESSIONAL_PATH, professional);
app.use(EXPERTISE_PATH, expertise);
app.use(COUNCIL_PATH, council);
app.use(LOGIN_PATH, login);

server.use(express.json());
server.use(BASE_PATH, app);
server.get(BASE_PATH, (req, res) => res.json({ message: "Welcome to Balder" }));

server.all(`${BASE_PATH}/*`, (req, res) =>
  res
    .status(CLIENT_ERROR.NOT_FOUND)
    .json({ code: CLIENT_ERROR.NOT_FOUND, message: "NOT FOUND" })
);

module.exports = server;
