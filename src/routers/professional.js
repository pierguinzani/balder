const { Router } = require("express");

const auth = require("../middlewares/auth");

const create = require("../routes/professional/create");
const update = require("../routes/professional/update");
const list = require("../routes/professional/list");
const remove = require("../routes/professional/remove");

const router = Router();

router.use(auth);

router.get("/", list);
router.post("/", create);
router.put("/", update);
router.delete("/", remove);

module.exports = router;
