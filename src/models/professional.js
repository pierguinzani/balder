const mongoose = require("mongoose");

const ObjectId = mongoose.Schema.Types.ObjectId;

const councilType = new mongoose.Schema({
  councilID: { type: ObjectId, required: true },
  number: { type: String, required: true },
});

const schema = new mongoose.Schema({
  name: { type: String, required: true },
  cpf: { type: String, required: true },
  user: { type: String, required: true },
  password: { type: String, required: true },
  profession: { type: ObjectId, required: true },
  expertise: { type: ObjectId, required: true },
  council: { type: councilType },
});

const model = mongoose.model("Professionals", schema);

module.exports = model;
