const mongoose = require("mongoose");

const schema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    uppercase: true,
    unique: true,
  },
});

const model = mongoose.model("Councils", schema);

module.exports = model;
