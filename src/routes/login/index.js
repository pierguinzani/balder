const jwt = require("jsonwebtoken");
const professionalModel = require("../../models/professional");
const {
  SUCCESS,
  CLIENT_ERROR,
  SERVER_ERROR,
} = require("../../utils/constants/status-code");

const { SECRET } = process.env;

const route = async (req, res) => {
  const { user, password } = req.body;

  try {
    const professional = await professionalModel.findOne({ user });

    if (!professional)
      return res
        .status(CLIENT_ERROR.UNAUTHORIZED)
        .json({ message: `${user} is unauthorized` });

    const logged = professional.password === password;

    if (!logged)
      return res
        .status(CLIENT_ERROR.FORBIDDEN)
        .json({ message: "Invalid password" });

    const { name, cpf, profession, expertise, council } = professional;
    const payload = { name, cpf, user, profession, expertise, council };

    const data = jwt.sign(payload, SECRET);

    return res
      .status(SUCCESS.OK)
      .json({ message: "Authenticated successfully", payload: data });
  } catch (err) {
    console.log(err);
    return res
      .status(SERVER_ERROR.INTERNAL_SERVER_ERROR)
      .json({ message: "Login internal server error" });
  }
};

module.exports = route;
