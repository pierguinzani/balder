const professionModel = require("../../models/profession");
const { SUCCESS, SERVER_ERROR } = require("../../utils/constants/status-code");

const route = async (req, res) => {
  const { id, ...data } = req.body;

  try {
    const result = await professionModel.findByIdAndUpdate(id, data, {
      new: true,
    });

    return res.status(SUCCESS.OK).json(result);
  } catch (err) {
    return res
      .status(SERVER_ERROR.INTERNAL_SERVER_ERROR)
      .json({ message: `Can't update profession with id ${id}` });
  }
};

module.exports = route;
