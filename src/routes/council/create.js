const councilModel = require("../../models/council");
const { SUCCESS, CLIENT_ERROR } = require("../../utils/constants/status-code");

const route = async (req, res) => {
  const data = req.body;

  try {
    const result = await councilModel.create(data);

    return res.status(SUCCESS.CREATED).json(result);
  } catch (err) {
    return res
      .status(CLIENT_ERROR.BAD_REQUEST)
      .json({ message: `Can't create a council with name ${data.name}` });
  }
};

module.exports = route;
