const expertiseModel = require("../../models/expertise");
const { SUCCESS, CLIENT_ERROR } = require("../../utils/constants/status-code");

const route = async (req, res) => {
  const data = req.body;

  try {
    const result = await expertiseModel.create(data);

    return res.status(SUCCESS.CREATED).json(result);
  } catch (err) {
    console.log(err);
    return res
      .status(CLIENT_ERROR.BAD_REQUEST)
      .json({ message: `Can't create an expertise with name ${data.name}` });
  }
};

module.exports = route;
