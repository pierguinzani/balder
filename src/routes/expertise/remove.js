const expertiseModel = require("../../models/expertise");
const { SUCCESS, SERVER_ERROR } = require("../../utils/constants/status-code");

const route = async (req, res) => {
  const { id } = req.body;

  try {
    const result = await expertiseModel.findByIdAndRemove(id);

    return res
      .status(SUCCESS.OK)
      .json({ message: `Expertise with id ${id} deleted`, result });
  } catch (err) {
    return res
      .status(SERVER_ERROR.INTERNAL_SERVER_ERROR)
      .json({ message: `Can't delete expertise with id ${id}` });
  }
};

module.exports = route;
