const professionalModel = require("../../models/professional");
const { SUCCESS, CLIENT_ERROR } = require("../../utils/constants/status-code");

const route = async (req, res) => {
  const data = req.body;

  try {
    const result = await professionalModel.create(data);

    return res.status(SUCCESS.CREATED).json(result);
  } catch (err) {
    const professional_example = {
      name: "<String>",
      cpf: "<String>",
      user: "<String>",
      password: "<String>",
      profession: "<ID>",
      expertise: "<ID>",
      council: {
        councilID: "<ID>",
        number: "<String>",
      },
    };

    return res
      .status(CLIENT_ERROR.BAD_REQUEST)
      .json({ message: `Can't create a professional`, professional_example });
  }
};

module.exports = route;
